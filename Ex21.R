# 2. Retrieve data from the first row and second column in the mtcars data frame: 


# 3. Examine the structure of the iris data frame, using the str() function:


# 4. What values can the Species factor in iris take (i.e., what are its levels)?


# 5. Examine the attributes in iris for their particular data type. Use one of:




# 6. Use row and column names instead of numeric coordinates to find out how many miles 
# per gallon (mpg) a Merc280C 2480 does (query the mtcars data set).


# 7. Find out how many rows are in the mtcars data frame:


# 8. Find out how many columns are in the data frame:


# 9. Preview the first few rows of the mtcars data frame: 


# 10. Use the c() function to select the mpg and gear attributes from mtcars:


# 11. Select the first and fifth through tenth variables of mtcars:


# 12. Exclude the variables mpg, cyl, and disp from the data set:



# 13. Exclude the third and fifth variables:


# 14. Delete the variables qsec and vs from a copy of mtcars:



# 15.Retrieve the ninth column vector of mtcars using the double 
# square bracket ([[]]) operator:


# 16. Retrieve the same column vector by its name: 


# 17. Use the $ operator instead of the double square bracket operator, to retrieve am:


# 18. Use a comma character with the [] operator to indicate all rows are to 
# be retrieved from the am column vector: 


# 19. Retrieve the first five complete rows of the data set:


# 20. Load the weather data set from the rattle package: 



# 21.Retrieve rows where the Rainfall in Canberra is greater than 16:


# 22. Use the attach() function to make objects within the data frame accessible 
# with fewer keystrokes and rerun the previous query with shorter syntax:



# 23. Select the Location, Date, and Rainfall columns for all rows where the 
# Rainfall is greater than or equal to 15:


# 24. Select all columns values between MinTemp and Sunshine (inclusive) where 
# WindGustDir is NW and it is raining on the following day:


