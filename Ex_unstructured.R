# 1. Load the rdmTweets.RData file into memory


# 2. Load the twitteR, wordcloud, SnowballC and tm libraries:



# 3. Convert the input to a data frame, and find the dimensionality of the data:



# 4. Create a corpus, and display the first six lines in the file:



# 5. Convert the text to lowercase, and display the first six lines in the file:



# 6. Remove all punctuation and display the first six lines in the file:



# 7. Remove numbers from the file and display the first six lines in the file:



# 8. Remove stop words from the file and display the first six lines in the file:



# 9. Take an original copy, perform stemming and display the first six lines in the file:



# 10. Complete the stemming process and display the first six lines in the file.
# This is complicated by a bug in the current version of the stemming package. Use
# the following function as a fix
stemCompletion_fix <- function(x, dictionary) 
  {
    PlainTextDocument(stripWhitespace(paste(
      stemCompletion(unlist(strsplit(as.character(x), " ")), 
                     dictionary=dictionary, type="shortest"), 
      sep="", collapse=" ")))
  }


# Complete the stemming process and display the first six lines in the file



# 11. Create a term document matrix and display a partial matrix:



# 12. Display frequent terms with a frequency of 10 or higher:


# 13. Convert the DTM to a matrix and calculate the word frequency.  
# Sort the frequencies and display as a bar plot where a word appears 
# in 15 or more tweets:




# 14. Build a word cloud where the size of the word is proportional to the 
# frequency of occurrence:


