# setwd("C:/1253/mktdata")

library(dplyr)
library(forecast)
library(lattice)
library(lubridate)
library(readxl)
library(TTR)
library(xts)

# Load data from Excel workbook

mktdata_file_name <- "mktdata.xlsx";

sheets <- excel_sheets(mktdata_file_name)

worksheets <- lapply(sheets, function(sheet) { read_excel(mktdata_file_name, sheet = sheet) })

readme_tbl <- worksheets[[1]]
assets_tbl <- worksheets[[2]]
portfolio_tbl <- worksheets[[3]]

# Explore data

head(readme_tbl)

head(assets_tbl)
str(assets_tbl)
summary(assets_tbl)

head(portfolio_tbl)
str(portfolio_tbl)
summary(portfolio_tbl)

# Review NAs

assets_tbl[apply(is.na(assets_tbl), MARGIN = 1, FUN = any), ]
portfolio_tbl[apply(is.na(portfolio_tbl), MARGIN = 1, FUN = any), ]

# Clean data---remove NAs

assets_tbl <- assets_tbl[complete.cases(assets_tbl), ]
portfolio_tbl <- portfolio_tbl[complete.cases(portfolio_tbl), ]

# Transform to time series objects

assets <- as.xts(assets_tbl[, 2:4], order.by = assets_tbl$DATE, dateFormat = "POSIXct")

head(assets)
str(assets)
summary(assets)

portfolio <- as.xts(portfolio_tbl[, 2:4], order.by = portfolio_tbl$DATE, dateFormat = "POSIXct")

head(portfolio)
str(portfolio)
summary(portfolio)

# Examine correlations

cor(assets)
cor(portfolio)

# Remove duplicate columns

assets <- assets[, -3]
portfolio <- portfolio[, -3]

# Plot time series

xyplot(assets)
xyplot(portfolio)

# Calculate returns

asset_returns <- TTR::ROC(assets)
xyplot(asset_returns)

portfolio_returns <- TTR::ROC(portfolio)
xyplot(portfolio_returns)

# Convert to ts objects

as_ts <- function(xts_data, frequency = 365) {
  start <- c(year(start(xts_data)), month(start(xts_data)))
  end <- c(year(end(xts_data)), month(end(xts_data)))
  
  ts(as.numeric(xts_data), start = start, end = end, frequency = frequency)
}

assets_ts <- as_ts(assets)
portfolio_ts <- as_ts(portfolio)

# Decompose into components

asset_components <- decompose(assets_ts)
plot(asset_components)

portfolio_components <- decompose(portfolio_ts)
plot(portfolio_components)

# Forecasting (non-stationary process)

assets_length <- length(index(assets))
assets_training_set_length <- floor(assets_length * 0.8)
assets_training_set <- assets[1:assets_training_set_length, ]
assets_test_set <- assets[(assets_training_set_length + 1):assets_length, ]

assets_training_set_ts <- as_ts(assets_training_set)
assets_test_set_ts <- as_ts(assets_test_set)

forecast_horizon <- length(index(assets_training_set))

assets_training_set_ts %>%
  forecast(h = forecast_horizon) %>%
  plot()

lines(assets_test_set_ts, col = "red")

# Holt-Winters

assets_training_set_ts %>%
  HoltWinters() %>%
  forecast(h = forecast_horizon) %>%
  plot()

lines(assets_test_set_ts, col = "red")

# Moving average

assets_training_set_ts %>%
  ma(order = 3, centre = TRUE) %>%
  forecast(h = forecast_horizon) %>%
  plot()

lines(assets_test_set_ts, col = "red")

# Accuracy

assets_forecast <- assets_training_set_ts %>%
  HoltWinters() %>%
  forecast(h = forecast_horizon)

accuracy(assets_forecast, assets_test_set_ts)

# Forecasting (returns)

asset_returns_length <- length(index(asset_returns))
asset_returns_training_set_length <- floor(asset_returns_length * 0.8)
asset_returns_training_set <- asset_returns[1:asset_returns_training_set_length, ]
asset_returns_test_set <- asset_returns[(asset_returns_training_set_length + 1):asset_returns_length, ]

asset_returns_training_set_ts <- as_ts(asset_returns_training_set)
asset_returns_test_set_ts <- as_ts(asset_returns_test_set)

asset_returns_forecast_horizon <- length(index(asset_returns_training_set))

asset_returns_forecast <- asset_returns_training_set_ts %>%
  forecast(h = asset_returns_forecast_horizon)

plot(asset_returns_forecast)

lines(asset_returns_test_set_ts, col = "red")

accuracy(asset_returns_forecast, asset_returns_test_set_ts)
