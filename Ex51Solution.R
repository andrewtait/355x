# 1. Import data
custchurn <- read.csv("custchurn.csv");

# 2. Examine the structure of the data frame
str(custchurn)

# 3. View the data set in a data sheet 
View(custchurn)

# 4. Examine the number of churners vs non-churners in the data
table(custchurn$churn)

# 5. Examine the proportion of these classifications in the data
prop.table(table(custchurn$churn))

# 6.  Set a seed to create replicatable random data
set.seed(12345)

# 7. Generate random numbers
custchurn_rand <- custchurn[order(runif(5000)), ]

# 8. Examine the first few rows in the randomised dataset and compare to the original
head(custchurn$account_length)

head(custchurn_rand$account_length)

# 9. Split the data into 90% training data and 10% test data
cust_train <- custchurn_rand[1:4500, ] 
cust_test <- custchurn_rand[4501:5000, ]

# 10. Check that the proportion of classes in the target value have been maintained in both the training and test data
prop.table(table(cust_train$churn))

prop.table(table(cust_test$churn))

# 11. Build the classification model, using the 21st column as the target value
library(C50)
cust_model <- C5.0(cust_train[-21], cust_train$churn)

# 12. Examine the model 
cust_model

# 12a. Confirm the number of observations that were used to create the classifier
# 4500

# 12b. How many features(variables) were used to create the classifier
# 20

# 12c. How many levels (decisions) are in the model
# 20

# 13. View the decision levels
summary(cust_model)

# 13a. What was the main splitting criterion? (the first decision)
# total_day_minutes

# 13b.  How many records are incorrectly classified?
# 212

# 13c. How many misclassified �No� values are there in the training model?
# 50

# 13d. How many misclassified �Yes� values are there in the training model?
# 162

# 14.  Apply the decision tree model to the test data
cust_predict <- predict(cust_model, cust_test)

# 15. Compare the vector of predicted class values to the actual class values
library(gmodels)

CrossTable(cust_test$churn,cust_predict)

# 16.  How accurate is the model when performed on the test data?
# 10 incorrectly classified non-churners
# 18 incorrectly classified churners

# 16a.  What was the error rate?
# 5.6%

# 16b. How does the performance of the model on the test data, compare to that of the model on the training data?
# Worse (4.7%)